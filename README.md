**Credenciales de acceso**

https://440133654360.signin.aws.amazon.com/console
	
	Usuario:christian
	password: keepcoding

	ID de CLAVE DE ACCESO: AKIAWM6QCS5MDBQMMF5U
	clave acceso secreta: 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


**Web de acceso a la aplicación**:

http://loadbalancerwebapp-1270688050.eu-west-1.elb.amazonaws.com:8080
	
	Solo es accesible por el balanceador.

**Entidades del entorno**

	Networking
		1 VPC - Practica-aws
		4 Subredes. 2 públicas(ec2) y 2 privadas (rds)
		1 Gateway - Practica-igw
		1 Balanceador de apps.

	EC2
		2 Instancias t2.micro. 
		Limitadas en entrada/salida por el security group Practica-EC2-sg.
		Entrada: ssh (22) y balanceador 8080
		webappa
		webappb
	
	RDS
		1 BBDD Mysql alojada en la 2 subredes privadas.
		name: mydb
		endpoint:mydb.cbqyvtizx5fh.eu-west-1.rds.amazonaws.com
		Limitada en su acceso, via ec2 (3306)


Para la autenticación de la APP con la BBDD, utilizamos el secreto de secretmanager "rtb-db-secret. 
Asignando a las instancias ec2, permisos de visualización de dicho secreto. 


SECURITY GROUPS

balancer_groupsecurity - balanceador

Practica-EC2-sg	- EC2

prueba			- RDS

En caso de degradación del servicio (caída un ec2, la APP seguirá levantada, pero solo funcionará por un nodo)

Si necesitas más permisos en AWS para revisar la práctica indícamelo.